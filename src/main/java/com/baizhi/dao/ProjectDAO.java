package com.baizhi.dao;


import com.baizhi.entity.Content;
import com.baizhi.entity.Project;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProjectDAO {

    List<Project> findAllProjects();

    int countProjects();

    void saveContent(Content content);

    Content findContentById(int id);
}
