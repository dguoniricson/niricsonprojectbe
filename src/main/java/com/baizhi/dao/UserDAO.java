package com.baizhi.dao;

import com.baizhi.entity.Accounts;
import com.baizhi.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper //用来创建DAO对象
public interface UserDAO {

    void updateAccount(Accounts accounts);

    void save(User user);

    void createAccount(Accounts account);

    User findByUserName(String username);

    List<Accounts> findAllAccounts();

    int countAccounts();

    List<Accounts> findAccountsByRegion(String country, String province);

    int countAccountsByRegion(String country, String province);


    void deleteAccountById(String id);


    List<Accounts> findAccountsById(String id);


}
