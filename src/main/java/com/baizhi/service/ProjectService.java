package com.baizhi.service;

import com.baizhi.entity.Content;
import com.baizhi.entity.Project;

import java.util.List;

public interface ProjectService {

    List<Project> findAllProjects();

    int countProjects();

    void saveContent(Content content);

    Content findContentById(int id);

}
