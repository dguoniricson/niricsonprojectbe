package com.baizhi.service;

import com.baizhi.dao.UserDAO;
import com.baizhi.entity.Accounts;
import com.baizhi.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public User login(User user) {
        User userDB = userDAO.findByUserName(user.getUsername());

        if(!ObjectUtils.isEmpty(userDB))
        {
            // compare password
            if(userDB.getPassword().equals(user.getPassword()))
            {
                return userDB;
            }else
            {
                throw new RuntimeException("password is incorrect");
            }
        }else{
            throw new RuntimeException("username is wrong");
        }
    }

    @Override
    public List<Accounts> findAllAccounts() {
        return userDAO.findAllAccounts();
    }

    @Override
    public int countAccounts() {
        return userDAO.countAccounts();
    }

    @Override
    public List<Accounts> findAccountsByRegion(String country, String province) {
        return userDAO.findAccountsByRegion(country,province);
    }

    @Override
    public int countAccountsByRegion(String country, String province) {
        return userDAO.countAccountsByRegion(country,province);
    }

    @Override
    public void deleteAccountById(String id) {
        userDAO.deleteAccountById(id);
    }

    @Override
    public List<Accounts> findAccountsById(String id) {
       return userDAO.findAccountsById(id);
    }

    @Override
    public void updateAccount(Accounts accounts) {
        userDAO.updateAccount(accounts);
    }

    @Override
    public void register(User user) {
        //0.根据用户输入用户名判断用户是否存在
        User userDB = userDAO.findByUserName(user.getUsername());
        if(userDB==null){
            //1.生成用户状态
            user.setStatus("已激活");
            //2.设置用户注册时间
            user.setRegisterTime(new Date());
            //3.调用DAO
            userDAO.save(user);
        }else{
            throw new RuntimeException("用户名已存在!");
        }
    }

    @Override
    public void createAccount(Accounts account) {
        userDAO.createAccount(account);
    }
}
