package com.baizhi.service;

import com.baizhi.entity.Accounts;
import com.baizhi.entity.Content;
import com.baizhi.entity.User;

import java.util.List;

public interface UserService {

    void updateAccount(Accounts accounts);

    void register(User user);

    void createAccount(Accounts account);


    User login(User user);

    List<Accounts> findAllAccounts();

    int countAccounts();

    List<Accounts> findAccountsByRegion(String country, String province);

    int countAccountsByRegion(String country, String province);

    void deleteAccountById(String id);

    List<Accounts> findAccountsById(String id);



}
