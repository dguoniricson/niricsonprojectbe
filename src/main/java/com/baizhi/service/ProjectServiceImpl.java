package com.baizhi.service;


import com.baizhi.dao.ProjectDAO;
import com.baizhi.entity.Content;
import com.baizhi.entity.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService{

    @Autowired
    private ProjectDAO projectDAO;


    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Project> findAllProjects() {

        return projectDAO.findAllProjects();
    }

    @Override
    public int countProjects() {
        return projectDAO.countProjects();
    }

    @Override
    public void saveContent(Content content) {
        content.setCreatetime(new Date());
        projectDAO.saveContent(content);
    }

    @Override
    public Content findContentById(int id) {
        return projectDAO.findContentById(id);
    }
}
