package com.baizhi.entity;


import lombok.Data;



@Data
public class Accounts{

    private String id;
    private String company;
    private String account;
    private String first;
    private String last;
    private String phone;
    private String email;
    private String country;
    private String province;
}
