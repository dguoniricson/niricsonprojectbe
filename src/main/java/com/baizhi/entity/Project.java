package com.baizhi.entity;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Project {
    private String id;
    private String projectname;
    private String status;
    private String photourl;
}
