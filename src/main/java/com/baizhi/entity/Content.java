package com.baizhi.entity;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class Content {
    private String id;
    private String contents;
    private Date createtime;
}
