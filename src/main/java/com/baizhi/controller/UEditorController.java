package com.baizhi.controller;

import com.example.demo.ActionEnter;
import org.json.JSONException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by ldb on 2017/4/9.
 */
@RestController
@CrossOrigin
public class UEditorController {


    private HttpServletRequest request;
    private HttpServletResponse response;
    private String rootPath = "";

    @RequestMapping("/")
    private String showPage(){
        return "index";
    }

    @RequestMapping(value="/config")
    public void config(HttpServletRequest request, HttpServletResponse response) throws JSONException {
        this.request = request;
        this.response = response;
        response.setContentType("application/json");
        rootPath = request.getSession().getServletContext().getRealPath("/");
        try {
            String exec = new ActionEnter(request, rootPath).exec();
            PrintWriter writer = response.getWriter();
            writer.write(exec);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @PostMapping(value = "/uploadimage")
    public Map<String, String> uploadimage(@RequestParam(value = "upfile") MultipartFile upfile) {
        Map<String, String> map = new HashMap<>();
        String fileName=upfile.getOriginalFilename();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String filename = sdf.format(new Date()) + new Random().nextInt(1000);
        String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
        filename=filename+"."+fileExt; // this is file name and extension
        File uploadedFile = new File(rootPath, filename);// saved path for the file
        try {
            upfile.transferTo(uploadedFile);//upload
            map.put("url", filename);// this is the url for the displayed image:  the full path of the
            //uploaded image is:  imageUrlPrefix+url
            map.put("state", "SUCCESS");
            map.put("original", "");
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return map;
    }

}
