package com.baizhi.controller;

import com.baizhi.entity.Content;
import com.baizhi.entity.User;
import com.baizhi.service.ProjectService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("project")
@CrossOrigin
public class ProjectController {

    @Autowired
    private ProjectService projectService;



    @PostMapping("saveContents")
    public Map<String, Object> saveContents(@RequestBody Map<String, String> content)
    {
        Map<String, Object> map = new HashMap<>();
        try {
            Content ct = new Content();
            ct.setContents(content.get("content"));
            projectService.saveContent(ct);
            map.put("state", true);
            map.put("msg", "save contents successfully");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("state", false);
            map.put("msg", "save contents failed");
        }

        return map;

    }


    @GetMapping("findContentById")
    public Map<String, Object> findAll(@RequestParam("id") int id)
    {
        Map<String, Object> map = new HashMap<>();

        try {
            map.put("content", projectService.findContentById(id).getContents());
            map.put("message", "content fectched");
        } catch (Exception e) {
            map.put("message", "content fectched failed");
            e.printStackTrace();
        }

        return map;

    }

    @GetMapping("findAllProjects")
    public Map<String, Object> findAll(@RequestParam("pageSize") int pageSize, @RequestParam("pageNumber") int pageNumber)
    {
        Map<String, Object> map = new HashMap<>();
        if(StringUtils.isEmpty(pageNumber))
        {
            map.put("List", projectService.findAllProjects());
        }else{
            PageHelper.startPage(pageNumber, pageSize);
            map.put("List", projectService.findAllProjects());
            map.put("total", projectService.countProjects());

        }

        return map;

    }
}
