package com.baizhi.controller;

import com.baizhi.entity.Accounts;
import com.baizhi.entity.User;
import com.baizhi.service.UserService;
import com.baizhi.utils.VerifyCodeUtils;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * deal with user login
     */
    @PostMapping("login")
    public Map<String, Object> login(@RequestBody User user)
    {
        log.info("current user info: [{}]", user);
        Map<String, Object> map = new HashMap<>();
        try {
            User userDB = userService.login(user);
            map.put("state", true);
            map.put("msg", "logged in successfully");
            map.put("user", userDB);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("state", false);
            map.put("msg", "username or password is wrong, please retry");
        }


        return map;
    }

    @GetMapping("findAllAccounts")
    public Map<String, Object> findAll(@RequestParam("pageSize") int pageSize, @RequestParam("pageNumber") int pageNumber)
    {
        Map<String, Object> map = new HashMap<>();
        if(StringUtils.isEmpty(pageNumber))
        {
            map.put("List", userService.findAllAccounts());
        }else{
            PageHelper.startPage(pageNumber, pageSize);
            map.put("List", userService.findAllAccounts());
            map.put("total", userService.countAccounts());

        }

        return map;

    }

    @GetMapping("findAccountsById")
    public Map<String, Object> findAll(@RequestParam("id") String id)
    {
        Map<String, Object> map = new HashMap<>();

        try {
            map.put("List", userService.findAccountsById(id));
            map.put("message", "account fectched");
        } catch (Exception e) {
            map.put("message", "account fectched failed");
            e.printStackTrace();
        }

        return map;

    }


    @PostMapping("saveAccount")
    public Map<String, Object> saveAccount(@RequestBody Accounts account)
    {
        Map<String, Object> map = new HashMap<>();

        if(userService.findAccountsById(account.getId()).size()>0)
        {
            // update account
            try {
                userService.updateAccount(account);
                map.put("message", "account updated");
                map.put("state", true);
            } catch (Exception e) {
                map.put("message", "account failed");
                map.put("state", false);
            }

        }else
        {
            //create account
            try {
//                String accoundId = UUID.randomUUID().toString();
//                account.setId(accoundId);
                userService.createAccount(account);
                map.put("message", "account created");
                map.put("state", true);
            } catch (Exception e) {
                map.put("message", "account created failed");
                map.put("state", false);
            }
        }


        return map;
    }



    @GetMapping("findAccountsByRegion")
    public Map<String, Object> findAll(
            @RequestParam("country") String country,
            @RequestParam("province") String province,
            @RequestParam("pageSize") int pageSize,
            @RequestParam("pageNumber") int pageNumber
            )
    {
        Map<String, Object> map = new HashMap<>();
        if (!StringUtils.isEmpty(pageNumber)) {
            PageHelper.offsetPage((pageNumber-1)*pageSize, pageSize);
        }
        map.put("List", userService.findAccountsByRegion(country,province));
        map.put("total", userService.countAccountsByRegion(country,province));
        return map;

    }



    @DeleteMapping("deleteAccountById")
    public Map<String, Object> deleteAccountById(
            @RequestParam("id") String id
    )
    {
        Map<String, Object> map = new HashMap<>();
        try {
            userService.deleteAccountById(id);
            map.put("message", "delete successfully");
        } catch (Exception e) {
            map.put("message", "delete failed");
            e.printStackTrace();
        }
        return map;

    }













    /**
     * 用来处理用户注册方法
     */
    @PostMapping("register")
    public Map<String, Object> register(@RequestBody User user, String code, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        try {
            String key = (String) request.getServletContext().getAttribute("code");
            userService.register(user);
            if (key.equalsIgnoreCase(code)) {
                userService.register(user);
                map.put("state", true);
                map.put("msg", "提示: 注册成功!");
            } else {
                throw new RuntimeException("验证码出现错误!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("state", false);
            map.put("msg", "提示:"+e.getMessage());
        }
        return map;
    }

    /**
     * verification code
     */
    @GetMapping("getImage")
    public String getImageCode(HttpServletRequest request) throws IOException {
        //1.使用工具类生成验证码
        String code = VerifyCodeUtils.generateVerifyCode(4);
        //2.将验证码放入servletContext作用域
        request.getServletContext().setAttribute("code", code);
        //3.将图片转为base64
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        VerifyCodeUtils.outputImage(120, 30, byteArrayOutputStream, code);
        return "data:image/png;base64," + Base64Utils.encodeToString(byteArrayOutputStream.toByteArray());
    }
}
